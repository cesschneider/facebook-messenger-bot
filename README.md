## Additional Interview Task: DevOps

To fully assess your skills the following scenario will have to be implemented by you:

We would like you to create a Facebook Messenger Chatbot that can be used for Meetups (https://www.meetup.com) and tracking how many people join a certain Meetup. 

Meetup.com has the following public open API: https://www.meetup.com/meetup_api/docs/stream/2/rsvps/#websockets which returns the current Meetups and RSVPs in real-time. We need you to build a system that stores this information and makes it searchable via Facebook Messenger.